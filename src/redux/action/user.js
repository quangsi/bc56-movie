import { userService } from "../../service/service";
import { SET_INFOR } from "../constant/user";
import toast from "react-hot-toast";

export const loginAction = (formData, callback) => {
  return (dispatch) => {
    userService
      .login(formData)
      .then((res) => {
        console.log(res);
        dispatch({
          type: SET_INFOR,
          payload: res.data.content,
        });
        toast.success("Đăng nhập thành công");
        localStorage.setItem("USER", JSON.stringify(res.data.content));
        callback();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
