import React from "react";
import { useSelector } from "react-redux";
import { PacmanLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerReducer);
  return isLoading ? (
    <div className="h-screen w-screen bg-black fixed z-10 flex justify-center items-center">
      <PacmanLoader size={200} speedMultiplier={3} color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}

// react spinner
