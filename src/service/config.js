import axios from "axios";
import { useDispatch } from "react-redux";
import { store } from "..";
import {
  turnOffLoadingAction,
  turnOnLoadingAction,
} from "../redux/action/spinner";

const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1MSIsIkhldEhhblN0cmluZyI6IjIzLzAyLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwODY0NjQwMDAwMCIsIm5iZiI6MTY4MDM2ODQwMCwiZXhwIjoxNzA4Nzk0MDAwfQ.m054V9MFrBY26j2t-FxqIXGcOVQim2UUk_G-OoewJUY";

export let https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
  },
});

// let dispatch = useDispatch();
// dispatch ngoài component

// axios interceptors
https.interceptors.request.use(
  function (config) {
    console.log("api đi");
    store.dispatch(turnOnLoadingAction());
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

https.interceptors.response.use(
  function (response) {
    console.log("api về");
    store.dispatch(turnOffLoadingAction());

    return response;
  },
  function (error) {
    store.dispatch(turnOffLoadingAction());

    return Promise.reject(error);
  }
);
