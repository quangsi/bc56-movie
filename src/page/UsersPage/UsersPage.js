import React, { useEffect, useState } from "react";
import { adminService } from "../../service/service";
import { Button, Table, Tag } from "antd";

export default function UsersPage() {
  const [userArr, setUserArr] = useState([]);
  // gọi api lấy danh sách ng dùng
  useEffect(() => {
    adminService
      .getUserList()
      .then((res) => {
        setUserArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  // antd table ( dùng table đầu tiên )

  // dataIndex ~ trùng với key của object trong data source
  const columns = [
    {
      title: "Name",
      dataIndex: "hoTen",
      key: "name",
    },
    {
      title: "Gmail",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "User Type",
      dataIndex: "maLoaiNguoiDung",
      key: "maLoaiNguoiDung",
      render: (text) => {
        if (text == "KhachHang") return <Tag color="green">Khách Hàng</Tag>;
        else return <Tag color="red">Quản trị</Tag>;
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: () => {
        return <Button danger>Delete</Button>;
      },
    },
  ];

  return (
    <div>
      <Table dataSource={userArr} columns={columns} />
    </div>
  );
}
