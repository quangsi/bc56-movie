import React, { useEffect } from "react";
import ListMovie from "./ListMovie/ListMovie";
import { movieService } from "../../service/service";
import TabMovie from "./TabMovie/TabMovie";

export default function HomePage() {
  return (
    <div>
      <ListMovie />
      <TabMovie />
    </div>
  );
}
